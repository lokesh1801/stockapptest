//
//  CollectionViewCell.swift
//  stockAppTest
//
//  Created by Sapan Yadav on 18/04/19.
//  Copyright © 2019 Sapan Yadav. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var splashView: UIView!
    @IBOutlet weak var splashImgView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.splashView.layer.cornerRadius = 13.0
        self.splashView.layer.shadowColor = UIColor.blue.cgColor
        self.splashView.layer.shadowOpacity = 0.5
        self.splashView.layer.shadowOffset = .zero
        self.splashView.layer.shadowPath = UIBezierPath(rect: self.splashView.bounds).cgPath
        self.splashView.layer.shouldRasterize = true
        // Initialization code
    }

}
