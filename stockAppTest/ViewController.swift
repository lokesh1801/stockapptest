//
//  ViewController.swift
//  stockAppTest
//
//  Created by Sapan Yadav on 18/04/19.
//  Copyright © 2019 Sapan Yadav. All rights reserved.
//

import UIKit
struct ModelCollectionFlowLayout {
    var title:String = ""
    var image:UIImage!
}
class ViewController: UIViewController {
    
    var arrData = [ModelCollectionFlowLayout]()

    @IBOutlet weak var splashCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectData()
        splashCollectionView.register(UINib.init(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        
        let floawLayout = UPCarouselFlowLayout()
        floawLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 60.0, height: splashCollectionView.frame.size.height)
        floawLayout.scrollDirection = .horizontal
        floawLayout.sideItemScale = 0.8
        floawLayout.sideItemAlpha = 1.0
        floawLayout.spacingMode = .fixed(spacing: 5.0)
        splashCollectionView.collectionViewLayout = floawLayout
        // Do any additional setup after loading the view.
    }
    
    func collectData(){
        arrData = [
            ModelCollectionFlowLayout(title: "splash1", image: #imageLiteral(resourceName: "astodiyaDarwaza")),
            ModelCollectionFlowLayout(title: "splash2", image: #imageLiteral(resourceName: "dariyapurDarwaza")),
            ModelCollectionFlowLayout(title: "splash3", image: #imageLiteral(resourceName: "delhiDarwaza")),
            ModelCollectionFlowLayout(title: "splash4", image: #imageLiteral(resourceName: "jamalpurDarwaza")),
            ModelCollectionFlowLayout(title: "splash5", image: #imageLiteral(resourceName: "premDarwaza")),
            ModelCollectionFlowLayout(title: "splash6", image: #imageLiteral(resourceName: "raipurDarwaza")),
            ModelCollectionFlowLayout(title: "", image: UIImage(named: ""))

        ]
    }
    

    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.splashCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
        if currentPage == 6 {
            pushToLoginPage(page: currentPage)
        }
    }
    func pushToLoginPage(page:Int) {
        if page == 6 {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let nav =  mainStoryBoard.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
    }
    fileprivate var currentPage: Int = 0 {
        didSet {
            print("page at centre = \(currentPage)")
        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.splashCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        cell.splashImgView.image = arrData[indexPath.row].image
//        cell.labelTitle.text = arrData[indexPath.row].title
        return cell
    }
    
    
    
}
