//
//  User.swift
//  RJT_19_FBLogin
//
//  Created by Mark on 12/28/17.
//  Copyright © 2017 Mark. All rights reserved.
//

import Foundation

struct User {
    var name: String
    var email: String
    var profileImage: URL
}
