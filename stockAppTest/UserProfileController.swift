//
//  UserProfileController.swift
//  RJT_19_FBLogin
//
//  Created by Mark on 12/28/17.
//  Copyright © 2017 Mark. All rights reserved.
//

import UIKit

import FBSDKLoginKit


class UserProfileController: UIViewController {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var logout: UIButton!
    
    var currentUser: User! {
        didSet {
            if view.window != nil {
                // update UI
                setupUI()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        updateUI()
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
        // logout
        FBSDKLoginManager().logOut()
    }
    
    @IBAction func homeBtnAction(_ sender: Any) {
        
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        present(profileVC, animated: true, completion: nil)
        
    }
    private func updateUI() {
        guard currentUser != nil else { return }
        name.text = currentUser.name
        birthday.text = "21/21/3232"
        email.text = currentUser.email
        
   
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let imageData = try Data(contentsOf: self.currentUser.profileImage)
                
                DispatchQueue.main.async {

                    self.profileImage.image = UIImage(data: imageData)
                }
                
            } catch let error {
                print(error)
            }
        }
    }
    
    private func setupUI() {
        logout.layer.cornerRadius = 10
        logout.clipsToBounds = true
    }
}
