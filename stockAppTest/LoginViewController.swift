//
//  LoginViewController.swift
//  stockAppTest
//
//  Created by Lokesh Yadav on 28/04/19.
//  Copyright © 2019 Sapan Yadav. All rights reserved.
//

import UIKit
import FBSDKLoginKit


class LoginViewController: UIViewController,FBSDKLoginButtonDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        let loginButton = FBSDKLoginButton(type: .system)
        loginButton.readPermissions = ["public_profile", "email"]
        loginButton.center = view.center
        loginButton.delegate = self
        //adding it to view
        view.addSubview(loginButton)
        
        //  If the user is already logged in aka. already has access token
        //  We represent each person logged into your app with AccessToken.current. The LoginManager sets this token for you and when it sets current it also automatically writes it to a keychain cache. The AccessToken contains userId which is used to identify the user
        if let _ = FBSDKAccessToken.current(){
            print("has access token already")
            getFBUserData()
        }
        

        // Do any additional setup after loading the view.
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if error == nil {
            getFBUserData()
        }
        print(result.grantedPermissions)
        
    }
    private func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil) {
            print("We have Access Token!!!!!")
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "birthday, email, first_name, last_name, picture.width(4096).height(4096)"]).start(completionHandler: { (connection, result, error) -> Void in
                print("Get back resposon!!!!!!!")
                if (error == nil){
                    print("Got User Data!!!!!")
                    print(result!)
                    
                    DispatchQueue.main.async {
                        print("fetch user Info")
                        self.fetchUserInfo(with: result as! [String : Any])
                    }
                }
            })
        }
    }
    
    func fetchUserInfo(with info: [String:Any]) {
        
        // construct a user
        guard let firstName = info["first_name"] as? String else { return }
        guard let lastName = info["last_name"] as? String else { return }
        guard let email = info["email"] as? String else { return }
        let url = info["picture"] as? [String:Any]
        let data = url?["data"] as?  [String:Any]
        let pictureURL = URL(string: (data?["url"] as? String)!)
        
        let fullName = firstName + " " + lastName
        let newUser = User(name: fullName, email: email, profileImage: pictureURL!)
        
        goToProfile(with: newUser)
        
    }
    // navigate to profile VC
    private func goToProfile(with currentUser: User) {
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "UserProfileController") as! UserProfileController
        profileVC.currentUser = currentUser
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    

}
